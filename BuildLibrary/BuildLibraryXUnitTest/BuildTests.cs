using BuildLibrary;
using System;
using System.Globalization;
using Xunit;

namespace BuildLibraryXUnitTest {
    public class BuildTests {

        //constants
        private const int ZERO = 0;
        private const int ONE = 1;
        private const int MAX = 9999;
        private const Build.BuildType BASE = Build.BuildType.Base;
        private const Build.BuildType ALPHA = Build.BuildType.Alpha;
        private const Build.BuildType BETA = Build.BuildType.Beta;
        private const Build.BuildType RELEASE = Build.BuildType.Release;


        #region Constructor Tests

        [Fact]
        public void EmptyConstructorTest() {
            Build build = new Build();
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == Build.BuildType.Base);
        }

        [Fact]
        public void ValuesConstructorTest() {
            //test major that is higher than 9999
            Assert.Throws<ArgumentOutOfRangeException>(
                () => new Build(10000, 0, 0));
            //test minor that is higher than 9999
            Assert.Throws<ArgumentOutOfRangeException>(
                () => new Build(0, 10000, 0));
            //test subMinor that is higher than 9999
            Assert.Throws<ArgumentOutOfRangeException>(
                () => new Build(0, 0, 10000));
            //test the values
            Build build = new Build(1, 2, 3, Build.BuildType.Alpha);
            Assert.True(build.Major == 1);
            Assert.True(build.Minor == 2);
            Assert.True(build.SubMinor == 3);
            Assert.True(build.Type == Build.BuildType.Alpha);
        }

        [Fact]
        public void StringConstructorTest() {
            Build build = new Build("10.11.12b");
            Assert.True(build.Major == 10);
            Assert.True(build.Minor == 11);
            Assert.True(build.SubMinor == 12);
            Assert.True(build.Type == Build.BuildType.Beta);
        }

        [Fact]
        public void CopyConstructorTest() {
            //create and copy
            Build build = new Build(4, 5, 6, Build.BuildType.Release);
            Build copy = new Build(build);
            //check values
            Assert.True(build.Major == copy.Major);
            Assert.True(build.Minor == copy.Minor);
            Assert.True(build.SubMinor == copy.SubMinor);
            Assert.True(build.Type == copy.Type);
            //modify values
            build.Major = 44;
            build.Minor = 55;
            build.SubMinor = 66;
            build.Type = Build.BuildType.Alpha;
            //check values
            Assert.False(build.Major == copy.Major);
            Assert.False(build.Minor == copy.Minor);
            Assert.False(build.SubMinor == copy.SubMinor);
            Assert.False(build.Type == copy.Type);
        }

        #endregion

        #region Properties Tests

        [Fact]
        public void MajorPropertyTest() {
            Build build = new Build("1.2.3a");
            Assert.True(build.Major == 1);
            build.Major = 4;
            Assert.True(build.Major == 4);
        }

        [Fact]
        public void MinorPropertyTest() {
            Build build = new Build("1.2.3a");
            Assert.True(build.Minor == 2);
            build.Minor = 5;
            Assert.True(build.Minor == 5);
        }

        [Fact]
        public void SubMinorPropertyTest() {
            Build build = new Build("1.2.3a");
            Assert.True(build.SubMinor == 3);
            build.SubMinor = 6;
            Assert.True(build.SubMinor == 6);
        }

        [Fact]
        public void TypePropertyTest() {
            Build build = new Build("1.2.3a");
            Assert.True(build.Type == Build.BuildType.Alpha);
            build.Type = Build.BuildType.Beta;
            Assert.True(build.Type == Build.BuildType.Beta);
        }

        [Fact]
        public void IsBasePropertyTest() {
            Build build = new Build("1.2.3");
            Assert.True(build.IsBase);
            build.Type = Build.BuildType.Alpha;
            Assert.False(build.IsBase);
            build.Type = Build.BuildType.Beta;
            Assert.False(build.IsBase);
            build.Type = Build.BuildType.Release;
            Assert.False(build.IsBase);
        }

        [Fact]
        public void IsAlphaPropertyTest() {
            Build build = new Build("1.2.3a");
            Assert.True(build.IsAlpha);
            build.Type = Build.BuildType.Base;
            Assert.False(build.IsAlpha);
            build.Type = Build.BuildType.Beta;
            Assert.False(build.IsAlpha);
            build.Type = Build.BuildType.Release;
            Assert.False(build.IsAlpha);
        }

        [Fact]
        public void IsBetaPropertyTest() {
            Build build = new Build("1.2.3b");
            Assert.True(build.IsBeta);
            build.Type = Build.BuildType.Base;
            Assert.False(build.IsBeta);
            build.Type = Build.BuildType.Alpha;
            Assert.False(build.IsBeta);
            build.Type = Build.BuildType.Release;
            Assert.False(build.IsBeta);
        }

        [Fact]
        public void IsReleasePropertyTest() {
            Build build = new Build("1.2.3r");
            Assert.True(build.IsRelease);
            build.Type = Build.BuildType.Base;
            Assert.False(build.IsRelease);
            build.Type = Build.BuildType.Alpha;
            Assert.False(build.IsRelease);
            build.Type = Build.BuildType.Beta;
            Assert.False(build.IsRelease);
        }

        [Fact]
        public void TypeStringPropertyTest() {
            Build build = new Build("1.2.3r");
            Assert.True(build.TypeString == "r");
            build.Type = Build.BuildType.Beta;
            Assert.True(build.TypeString == "b");
            build.Type = Build.BuildType.Alpha;
            Assert.True(build.TypeString == "a");
            build.Type = Build.BuildType.Base;
            Assert.True(build.TypeString == "");
        }

        #endregion

        #region Constants

        [Fact]
        public void ZeroBaseTest() {
            Build build = Build.ZeroBase;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void ZeroAlphaTest() {
            Build build = Build.ZeroAlpha;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == ALPHA);
        }

        [Fact]
        public void ZeroBetaTest() {
            Build build = Build.ZeroBeta;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BETA);
        }

        [Fact]
        public void ZeroReleaseTest() {
            Build build = Build.ZeroRelease;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == RELEASE);
        }

        [Fact]
        public void MinBaseTest() {
            Build build = Build.MinBase;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MinAlphaTest() {
            Build build = Build.MinAlpha;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == ALPHA);
        }

        [Fact]
        public void MinBetaTest() {
            Build build = Build.MinBeta;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BETA);
        }

        [Fact]
        public void MinReleaseTest() {
            Build build = Build.MinRelease;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == RELEASE);
        }

        [Fact]
        public void OneBaseTest() {
            Build build = Build.OneBase;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void OneAlphaTest() {
            Build build = Build.OneAlpha;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == ALPHA);
        }

        [Fact]
        public void OneBetaTest() {
            Build build = Build.OneBeta;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == BETA);
        }

        [Fact]
        public void OneReleaseTest() {
            Build build = Build.OneRelease;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == RELEASE);
        }

        [Fact]
        public void MaxBaseTest() {
            Build build = Build.MaxBase;
            Assert.True(build.Major == MAX);
            Assert.True(build.Minor == MAX);
            Assert.True(build.SubMinor == MAX);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MaxAlphaTest() {
            Build build = Build.MaxAlpha;
            Assert.True(build.Major == MAX);
            Assert.True(build.Minor == MAX);
            Assert.True(build.SubMinor == MAX);
            Assert.True(build.Type == ALPHA);
        }

        [Fact]
        public void MaxBetaTest() {
            Build build = Build.MaxBeta;
            Assert.True(build.Major == MAX);
            Assert.True(build.Minor == MAX);
            Assert.True(build.SubMinor == MAX);
            Assert.True(build.Type == BETA);
        }

        [Fact]
        public void MaxReleaseTest() {
            Build build = Build.MaxRelease;
            Assert.True(build.Major == MAX);
            Assert.True(build.Minor == MAX);
            Assert.True(build.SubMinor == MAX);
            Assert.True(build.Type == RELEASE);
        }

        [Fact]
        public void MajorBuildTest() {
            Build build = Build.MajorBuild;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MinorBuildTest() {
            Build build = Build.MinorBuild;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void SubMinorBuildTest() {
            Build build = Build.SubMinorBuild;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MajorMinorBuildTest() {
            Build build = Build.MajorMinorBuild;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ZERO);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MajorSubMinorBuildTest() {
            Build build = Build.MajorSubMinorBuild;
            Assert.True(build.Major == ONE);
            Assert.True(build.Minor == ZERO);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == BASE);
        }

        [Fact]
        public void MinorSubMinorBuild() {
            Build build = Build.MinorSubMinorBuild;
            Assert.True(build.Major == ZERO);
            Assert.True(build.Minor == ONE);
            Assert.True(build.SubMinor == ONE);
            Assert.True(build.Type == BASE);
        }


        #endregion

        #region Method Tests

        [Fact]
        public void IsBuildTest() {
            Assert.False(Build.IsBuild(""));
            Assert.False(Build.IsBuild("0"));
            Assert.False(Build.IsBuild("0."));
            Assert.False(Build.IsBuild("0.0"));
            Assert.False(Build.IsBuild("0.0."));
            Assert.False(Build.IsBuild("0,0,0a"));
            Assert.False(Build.IsBuild(null));
            Assert.False(Build.IsBuild(" "));
            Assert.True(Build.IsBuild("0.0.0"));
            Assert.True(Build.IsBuild("0.0.0a"));
            Assert.True(Build.IsBuild("0.0.0b"));
            Assert.True(Build.IsBuild("0.0.0r"));
            Assert.True(Build.IsBuild("9999.9999.9999"));
            Assert.True(Build.IsBuild("9999.9999.9999a"));
            Assert.True(Build.IsBuild("9999.9999.9999b"));
            Assert.True(Build.IsBuild("9999.9999.9999r"));
        }

        [Fact]
        public void CheckTypedText() {
            Assert.False(Build.CheckTypedText("sdfs45s3d1f"));
            Assert.False(Build.CheckTypedText("0,0,0a"));
            Assert.False(Build.CheckTypedText(" "));
            Assert.False(Build.CheckTypedText(null));
            Assert.True(Build.CheckTypedText("9999.9999.9999"));
            Assert.True(Build.CheckTypedText("9999.9999.9999a"));
            Assert.True(Build.CheckTypedText("9999.9999.9999b"));
            Assert.True(Build.CheckTypedText("9999.9999.9999r"));
            Assert.True(Build.CheckTypedText("0.0.0"));
            Assert.True(Build.CheckTypedText("0.0.0a"));
            Assert.True(Build.CheckTypedText("0.0.0b"));
            Assert.True(Build.CheckTypedText("0.0.0r"));
            Assert.True(Build.CheckTypedText("165"));
            Assert.True(Build.CheckTypedText("0"));
            Assert.True(Build.CheckTypedText("10"));
            Assert.True(Build.CheckTypedText("100"));
            Assert.True(Build.CheckTypedText("9999"));
            Assert.True(Build.CheckTypedText("9999."));
            Assert.True(Build.CheckTypedText("0."));
            Assert.True(Build.CheckTypedText("0.0"));
            Assert.True(Build.CheckTypedText("0.10"));
            Assert.True(Build.CheckTypedText("0.100"));
            Assert.True(Build.CheckTypedText("0.9999"));
            Assert.True(Build.CheckTypedText("0.9999."));
            Assert.True(Build.CheckTypedText("0.0."));
            Assert.True(Build.CheckTypedText("0.0.0"));
            Assert.True(Build.CheckTypedText("0.0.10"));
            Assert.True(Build.CheckTypedText("0.0.100"));
            Assert.True(Build.CheckTypedText("0.0.9999"));
        }

        [Fact]
        public void CopyTest() {
            //create variables
            Build a = new Build("12.23.34r");
            Build b = a.Copy();
            //test
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
            //change and test
            a.Major = MAX;
            Assert.False(a.Equals(b));
            Assert.False(b.Equals(a));
        }

        [Fact]
        public void CompareToTest() {
            //create variables
            Build a = new Build(1, 1, 1, ALPHA);
            Build b = new Build(1, 1, 2, ALPHA);
            //test
            Assert.True(a.CompareTo(b) == -1);
            Assert.True(b.CompareTo(a) == 1);
            Assert.False(a.CompareTo(b) == 0);
            Assert.True(a.CompareTo(a) == 0);
        }

        [Fact]
        public void ToStringTest() {
            //check base
            string str = "1.2.3";
            Build build = new Build(str);
            Assert.Equal(str, build.ToString());
            //check alpha
            str = "4.5.6a";
            build = new Build(str);
            Assert.Equal(str, build.ToString());
            //check beta
            str = "7.8.9b";
            build = new Build(str);
            Assert.Equal(str, build.ToString());
            //check release
            str = "10.11.12r";
            build = new Build(str);
            Assert.Equal(str, build.ToString());
        }

        [Fact]
        public void EqualsTest() {
            Build a = new Build(1, 2, 3, ALPHA);
            Build b = new Build("1.2.3a");
            //test the values
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
            Assert.True(a.Equals("1.2.3a"));
            Assert.True(b.Equals("1.2.3a"));
            //change and test
            b.Major = MAX;
            Assert.False(a.Equals(b));
            Assert.False(b.Equals(a));
            a.Major = MAX;
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
            b.Minor = MAX;
            Assert.False(a.Equals(b));
            Assert.False(b.Equals(a));
            a.Minor = MAX;
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
            b.SubMinor = MAX;
            Assert.False(a.Equals(b));
            Assert.False(b.Equals(a));
            a.SubMinor = MAX;
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
            b.Type = BETA;
            Assert.False(a.Equals(b));
            Assert.False(b.Equals(a));
            a.Type = BETA;
            Assert.True(a.Equals(b));
            Assert.True(b.Equals(a));
        }

        [Fact]
        public void GetHashCodeTest() {
            Build build = new Build("1.2.3a");
            Assert.Equal(build.GetHashCode(), build.ToString().GetHashCode(StringComparison.InvariantCulture));
        }

        #endregion

        #region Bool Operator Tests

        [Fact]
        public void LessThanTest() {
            Build a = new Build("0.0.1");
            Build b = new Build("0.0.2");
            Build c = null;
            Assert.True(c < a);
            Assert.True(a < "0.0.2");
            Assert.True("0.0.1" < b);
            Assert.True(a < b);
            b.Type = ALPHA;
            Assert.True(a < b);
            a.Type = BETA;
            Assert.True(b < a);
            b.Type = RELEASE;
            Assert.True(a < b);
            c = a.Copy();
            Assert.False(a < c);
        }

        [Fact]
        public void GreaterThanTest() {
            Build a = new Build("0.0.2");
            Build b = new Build("0.0.1");
            Build c = null;
            Assert.True(a > c);
            Assert.True(a > "0.0.1");
            Assert.True("0.0.2" > b);
            Assert.True(a > b);
            b.Type = ALPHA;
            Assert.True(b > a);
            a.Type = BETA;
            Assert.True(a > b);
            b.Type = RELEASE;
            Assert.True(b > a);
            c = a.Copy();
            Assert.False(a > c);
        }

        [Fact]
        public void LessThanOrEqualTest() {
            Build a = new Build("0.0.1");
            Build b = new Build("0.0.2");
            Build c = null;
            Assert.True(c <= a);
            Assert.True(a <= "0.0.2");
            Assert.True("0.0.1" <= b);
            Assert.True(a <= b);
            b.Type = ALPHA;
            Assert.True(a <= b);
            a.Type = BETA;
            Assert.True(b <= a);
            b.Type = RELEASE;
            Assert.True(a <= b);
            c = a.Copy();
            Assert.True(a <= c);
        }

        [Fact]
        public void GreaterThanOrEqualTest() {
            Build a = new Build("0.0.2");
            Build b = new Build("0.0.1");
            Build c = null;
            Assert.True(a >= c);
            Assert.True(a >= "0.0.1");
            Assert.True("0.0.2" >= b);
            Assert.True(a >= b);
            b.Type = ALPHA;
            Assert.True(b >= a);
            a.Type = BETA;
            Assert.True(a >= b);
            b.Type = RELEASE;
            Assert.True(b >= a);
            c = a.Copy();
            Assert.True(a >= c);
        }

        [Fact]
        public void EqualEqualTest() {
            Build a = new Build(0, 1, 2);
            Build b = new Build("0.1.2");
            Build c = null;
            Build d = null;
            Assert.True(a == "0.1.2");
            Assert.True(a == b);
            Assert.True(c == null);
            Assert.True(c == d);
            Assert.False(a == null);
            Assert.False(a == c);
        }

        [Fact]
        public void NotEqualTest() {
            Build a = new Build(0, 1, 2);
            Build b = new Build("0.1.2");
            Build c = null;
            Build d = null;
            Assert.False(a != "0.1.2");
            Assert.False(a != b);
            Assert.False(c != null);
            Assert.False(c != d);
            Assert.True(a != null);
            Assert.True(a != c);
        }

        #endregion

        #region Arithmetic Operator Tests

        [Fact]
        public void AddTest() {
            //create Variables
            Build a = new Build("0.0.0");
            Build b = new Build("0.0.1");
            Build c = new Build("0.0.9999");
            Build d = new Build("0.9999.9999");
            Build e = new Build("9999.9999.9999");
            //Test different Methods of adding
            Assert.True((a + 1).Equals(b));
            Assert.True((a + "0.0.1").Equals(b));
            Assert.True(("0.0.0" + b).Equals(b));
            Assert.True((a + b).Equals(b));
            //check that values greater than 9999 are handled correctly
            Assert.True((c + b).Equals("0.1.0"));
            Assert.True((d + b).Equals("1.0.0"));
            Assert.Throws<ArgumentOutOfRangeException>(
                () => e + b);
            //check that the first values type is kept for the result
            Build al = new Build("1.1.1a");
            Build bt = new Build("1.1.1b");
            Assert.True((al + bt).Equals("2.2.2a"));
            Assert.True((bt + al).Equals("2.2.2b"));
        }

        [Fact]
        public void PlusPlusTest() {
            Build a = new Build("0.0.1");
            Build b = new Build("0.0.2");
            Build c = new Build("0.0.9999");
            Build d = new Build("0.1.0");
            a++;
            Assert.True(a.Equals(b));
            c++;
            Assert.True(c.Equals(d));
        }

        [Fact]
        public void SubtractTest() {
            //create Variables
            Build a = new Build("0.0.0");
            Build b = new Build("0.0.1");
            Build c = new Build("0.0.9999");
            Build d = new Build("0.9999.9999");
            Build e = new Build("9999.9999.9999");
            //Test different Methods of adding
            Assert.True((b - 1).Equals(a));
            Assert.True((b - "0.0.1").Equals(a));
            Assert.True(("0.0.1" - b).Equals(a));
            Assert.True((b - b).Equals(a));
            //check that values greater than 9999 are handled correctly
            Assert.True(("0.1.0" - b).Equals(c));
            Assert.True(("1.0.0" - b).Equals(d));
            Assert.Throws<ArgumentOutOfRangeException>(
                () => a - b);
            //check that the first values type is kept for the result
            Build al = new Build("1.1.1a");
            Build bt = new Build("1.1.1b");
            Assert.True((al - bt).Equals("0.0.0a"));
            Assert.True((bt - al).Equals("0.0.0b"));
        }

        [Fact]
        public void MinusMinusTest() {
            Build a = new Build("0.0.1");
            Build b = new Build("0.0.2");
            Build c = new Build("0.0.9999");
            Build d = new Build("0.1.0");
            b--;
            Assert.True(a.Equals(b));
            d--;
            Assert.True(c.Equals(d));
        }

        [Fact]
        public void MultiplyTest() {
            Build a = new Build(2, 3, 4);
            Build b = new Build(4, 9, 16);
            Build c = new Build(9000, 9000, 9000);
            Build d = new Build(9999, 9999, 9999);
            Build e = new Build(4, 6, 8);

            Assert.Equal((a * a).ToString(), b.ToString());
            Assert.Equal((c * c).ToString(), d.ToString());
            Assert.Equal((c * a).ToString(), d.ToString());
            Assert.Equal((a * "2.3.4").ToString(), b.ToString());
            Assert.Equal(("2.3.4" * a).ToString(), b.ToString());
            Assert.Equal((a * 2).ToString(), e.ToString());
        }

        [Fact]
        public void DivideTest() {
            Build a = new Build(2, 3, 4);
            Build b = new Build(4, 9, 16);
            Build c = new Build(5000, 3333, 2500);
            Build d = new Build(9999, 9999, 9999);
            Build e = new Build(0, 0, 0);
            Build f = new Build(3333, 3333, 3333);
            //check division            
            Assert.Equal((b / a).ToString(), a.ToString());
            Assert.Equal(("4.9.16" / a).ToString(), a.ToString());
            Assert.Equal((b / "2.3.4").ToString(), a.ToString());
            Assert.Equal((d / a).ToString(), c.ToString());
            Assert.Equal((d / 3).ToString(), f.ToString());
            //a/0 should return 0 for saftey
            Assert.Equal((a / 0).ToString(), e.ToString());
        }

        #endregion

    }
}
