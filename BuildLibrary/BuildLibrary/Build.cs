﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Diagnostics.CodeAnalysis;

namespace BuildLibrary {

    /// <summary>
    /// This class is used to represent a build version.
    /// </summary>
    public class Build : IComparable<Build> {

        #region Enums

        /// <summary>
        /// This enum is used as a representation for the <c>Build</c>'s type.
        /// </summary>
        public enum BuildType {

            /// <summary>
            /// Base Build Type.  This <c>BuildType</c> is in the format of "0.0.0" and
            /// is used for applications that do not need different build types.  This
            /// build type is seen as less than any of the other build types.
            /// </summary>
            Base = 1,

            /// <summary>
            /// Alpha Build Type.  This <c>BuildType</c> is in the format of "0.0.0a" and
            /// is used for application versions that have not been tested and may be
            /// unstable.
            /// </summary>
            Alpha = 2,

            /// <summary>
            /// Beta Build Type.  This <c>BuildType</c> is in the format of "0.0.0b" and 
            /// is used for application versions that are ready to be tested by many people
            /// but still may be unstable.
            /// </summary>
            Beta = 3,

            /// <summary>
            /// Release Build Type.  This <c>BuildType</c> is in the format of "0.0.0r" and
            /// is used for application versions that have been tested and should be stable.
            /// </summary>
            Release = 4
        }

        #endregion

        #region Properties

        /// <summary>
        /// This value is the first number in the <c>Build</c>.  Its value
        /// should be in the range 0 to 9999.
        /// </summary>
        public ushort Major { get; set; }

        /// <summary>
        /// This value is the second number in the <c>Build</c>. Its value
        /// should be in the range 0 to 9999.
        /// </summary>
        public ushort Minor { get; set; }

        /// <summary>
        /// This value is the third and last number in the <c>Build</c>.  Its
        /// value should be in the range 0 to 9999.
        /// </summary>
        public ushort SubMinor { get; set; }

        /// <summary>
        /// This value contains the <c>BuildType</c> of the <c>Build</c>.
        /// </summary>
        public BuildType Type { get; set; }

        /// <summary>
        /// This value is <c>True</c> when the <b>Build</b> has a Base <c>Type</c>.
        /// </summary>
        public bool IsBase { get { return Type == BuildType.Base; } }

        /// <summary>
        /// This value is <c>True</c> when the <b>Build</b> has an Alpha <c>Type</c>.
        /// </summary>
        public bool IsAlpha { get { return Type == BuildType.Alpha; } }

        /// <summary>
        /// This value is <c>True</c> when the <b>Build</b> has a Beta <c>Type</c>.
        /// </summary>
        public bool IsBeta { get { return Type == BuildType.Beta; } }

        /// <summary>
        /// This value is <c>True</c> when the <b>Build</b> has a Release <c>Type</c>.
        /// </summary>
        public bool IsRelease { get { return Type == BuildType.Release; } }

        /// <summary>
        /// This value contains the <c>BuildType</c> indicator as a string.
        /// </summary>
        public string TypeString {
            get {
                return Type switch
                {
                    BuildType.Base => "",
                    BuildType.Alpha => "a",
                    BuildType.Beta => "b",
                    BuildType.Release => "r",
                    _ => ""
                };
            }
        }

        #endregion

        #region Static Instances

        /// <summary>
        /// <c>Build</c> "0.0.0"
        /// </summary>
        public static Build ZeroBase { get { return new Build("0.0.0"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0a"
        /// </summary>
        public static Build ZeroAlpha { get { return new Build("0.0.0a"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0b"
        /// </summary>
        public static Build ZeroBeta { get { return new Build("0.0.0b"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0r"
        /// </summary>
        public static Build ZeroRelease { get { return new Build("0.0.0r"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0"
        /// </summary>
        public static Build MinBase { get { return new Build("0.0.0"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0a"
        /// </summary>
        public static Build MinAlpha { get { return new Build("0.0.0a"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0b"
        /// </summary>
        public static Build MinBeta { get { return new Build("0.0.0b"); } }

        /// <summary>
        /// <c>Build</c> "0.0.0r"
        /// </summary>
        public static Build MinRelease { get { return new Build("0.0.0r"); } }

        /// <summary>
        /// <c>Build</c> "1.1.1"
        /// </summary>
        public static Build OneBase { get { return new Build("1.1.1"); } }

        /// <summary>
        /// <c>Build</c> "1.1.1a"
        /// </summary>
        public static Build OneAlpha { get { return new Build("1.1.1a"); } }

        /// <summary>
        /// <c>Build</c> "1.1.1b"
        /// </summary>
        public static Build OneBeta { get { return new Build("1.1.1b"); } }

        /// <summary>
        /// <c>Build</c> "1.1.1r"
        /// </summary>
        public static Build OneRelease { get { return new Build("1.1.1r"); } }

        /// <summary>
        /// <c>Build</c> "9999.9999.9999"
        /// </summary>
        public static Build MaxBase { get { return new Build("9999.9999.9999"); } }

        /// <summary>
        /// <c>Build</c> "9999.9999.9999a"
        /// </summary>
        public static Build MaxAlpha { get { return new Build("9999.9999.9999a"); } }

        /// <summary>
        /// <c>Build</c> "9999.9999.9999b"
        /// </summary>
        public static Build MaxBeta { get { return new Build("9999.9999.9999b"); } }

        /// <summary>
        /// <c>Build</c> "9999.9999.9999r"
        /// </summary>
        public static Build MaxRelease { get { return new Build("9999.9999.9999r"); } }

        /// <summary>
        /// <c>Build</c> "1.0.0"
        /// </summary>
        public static Build MajorBuild { get { return new Build("1.0.0"); } }

        /// <summary>
        /// <c>Build</c> "0.1.0"
        /// </summary>
        public static Build MinorBuild { get { return new Build("0.1.0"); } }

        /// <summary>
        /// <c>Build</c> "0.0.1"
        /// </summary>
        public static Build SubMinorBuild { get { return new Build("0.0.1"); } }

        /// <summary>
        /// <c>Build</c> "1.1.0"
        /// </summary>
        public static Build MajorMinorBuild { get { return new Build("1.1.0"); } }

        /// <summary>
        /// <c>Build</c> "1.0.1"
        /// </summary>
        public static Build MajorSubMinorBuild { get { return new Build("1.0.1"); } }

        /// <summary>
        /// <c>Build</c> "0.1.1"
        /// </summary>
        public static Build MinorSubMinorBuild { get { return new Build("0.1.1"); } }

        #endregion

        #region Constructors

        /// <summary>
        /// This constructor is used to create a default <c>Build</c> of "0.0.0".
        /// </summary>
        public Build() {
            Major = 0;
            Minor = 0;
            SubMinor = 0;
            Type = BuildType.Base;
        }

        /// <summary>
        /// This method is used to create a <c>Build</c> with the given values.
        /// </summary>
        /// <param name="major">The first number in the <c>Build</c>.</param>
        /// <param name="minor">The second number in the <c>Build</c>.</param>
        /// <param name="subMinor">The third and final number in the <c>Build</c>.</param>
        /// <param name="type">The <c>BuildType</c> of the <c>Build</c>.</param>
        /// <exception cref="ArgumentOutOfRangeException">When one of the passed
        /// numbers is not in the range of 0 to 9999.</exception>
        public Build(ushort major, ushort minor, ushort subMinor, BuildType type = BuildType.Base) {
            ValidateConstructor(major, minor, subMinor);
            Major = major;
            Minor = minor;
            SubMinor = subMinor;
            Type = type;
        }

        /// <summary>
        /// This method is used to create a <c>Build</c> whith the given string.
        /// </summary>
        /// <param name="buildString">The <c>Build</c> string.</param>
        /// <param name="force">When set to <c>True</c> a default <c>Build</c> will
        /// be returned if <paramref name="buildString"/> is invalid.  When set to <c>False</c> an
        /// exeption will be thrown if <paramref name="buildString"/> is invalid.</param>
        /// <exception cref="ArgumentException">When the passed string is invalid and 
        /// <paramref name="force"/> is not <c>True</c>.</exception>
        public Build(string buildString, bool force = false) {
            if (!IsBuild(buildString)) { //the passed string is bad
                if (force) {
                    Major = 0;
                    Minor = 0;
                    SubMinor = 0;
                    Type = BuildType.Base;
                } else {
                    throw new ArgumentException("A Build object cannot be created using the string \"" +
                        buildString + "\"", nameof(buildString));
                }
            } else { //the passed string is good
                //get the last character to get the build type
                char last = char.ToLower(buildString.ToCharArray()[buildString.Length - 1], CultureInfo.InvariantCulture);
                if (char.IsLetter(last)) {
                    Type = last switch {
                        'a' => BuildType.Alpha,
                        'b' => BuildType.Beta,
                        'r' => BuildType.Release,
                        _ => BuildType.Base,
                    };
                    buildString = buildString[0..^1];
                } else {
                    Type = BuildType.Base;
                }
                //get the numbers (we know the numbers are valid from the IsBuild())
                string[] numbers = buildString.Split('.');
                Major = ushort.Parse(numbers[0], CultureInfo.InvariantCulture);
                Minor = ushort.Parse(numbers[1], CultureInfo.InvariantCulture);
                SubMinor = ushort.Parse(numbers[2], CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// This method is used to help with some of the opperators and will never
        /// be used outside this class.
        /// </summary>
        /// <param name="i">Contains a number you want to parse as a <c>Build</c>.</param>
        private Build(int i) {
            int major = 0, minor = 0, subminor = i;
            if (subminor > 9999) {
                minor = subminor / 9999;
                subminor %= 9999;
            }
            if (minor > 9999) {
                major = minor / 9999;
                minor %= 9999;
            }
            if (major > 9999) throw new ArgumentOutOfRangeException(nameof(i));
            Major = (ushort)major;
            Minor = (ushort)minor;
            SubMinor = (ushort)subminor;
            Type = BuildType.Base;
        }

        /// <summary>
        /// This method is used to create a new copy of <paramref name="build"/>.
        /// </summary>
        /// <param name="build">The <c>Build</c> you want to copy.</param>
        public Build(Build build) {
            if (build == null) throw new ArgumentNullException(nameof(build));
            Major = build.Major;
            Minor = build.Minor;
            SubMinor = build.SubMinor;
            Type = build.Type;
        }

        #endregion

        #region Regex

        /// <summary>
        /// This value is used to test if a <c>string</c> is a valid <c>Build</c>.
        /// </summary>
        private static readonly Regex BuildRegex = new Regex(@"\A[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}[abr]?\Z");

        /// <summary>
        /// This value is used to check if a typed value is valid to makining a valid
        /// <c>Build</c> string.
        /// </summary>
        private static readonly Regex TypeCheck = new Regex(@"\A[0-9]{1,4}\.[0-9]{1,4}\.[0-9]{1,4}[abr]?\Z|\A[0-9]{1,4}\.[0-9]{1,4}\.?\Z|\A[0-9]{1,4}\.?\Z");

        #endregion

        #region Validation

        /// <summary>
        /// This method is used to validate the values that are passed to a constructor.
        /// </summary>
        /// <param name="major">The first number in the <c>Build</c>.</param>
        /// <param name="minor">The second number in the <c>Build</c></param>
        /// <param name="subMinor">The last number in the <b>Build</b></param>
        /// <exception cref="ArgumentOutOfRangeException">When one of the passed values
        /// is not in the range of 0 and 9999.</exception>
        private static void ValidateConstructor(ushort major, ushort minor, ushort subMinor) {
            string exception = $"The build {major}.{minor}.{subMinor} can not be created " +
                "using numbers less than 0 or greater than 9999.";
            bool invalid = false;
            string name = "";
            string value = "";
            if (major < 0 || major > 9999) {
                invalid = true;
                name = nameof(major);
                value = major.ToString(CultureInfo.InvariantCulture);
            }
            if (minor < 0 || minor > 9999) {
                invalid = true;
                name = nameof(minor);
                value = minor.ToString(CultureInfo.InvariantCulture);
            }
            if (subMinor < 0 || subMinor > 9999) {
                invalid = true;
                name = nameof(subMinor);
                value = minor.ToString(CultureInfo.InvariantCulture);
            }
            if (invalid) {
                throw new ArgumentOutOfRangeException(name, value, exception);
            }
        }

        /// <summary>
        /// This method is used to check if a <c>string</c> is a valid <c>Build</c>.
        /// </summary>
        /// <param name="build">The string you want to check.</param>
        /// <returns><c>True</c> is <paramref name="build"/> contains a valid <c>Build</c>,
        /// otherwise returns <c>False</c>.</returns>
        public static bool IsBuild(string build) {
            if (string.IsNullOrEmpty(build)) return false;
            return BuildRegex.IsMatch(build);
        }
        
        /// <summary>
        /// This method is used to check if some typed text is valid for creating a <c>Build</c>.
        /// </summary>
        /// <param name="text">This <c>string</c> you want to check.</param>
        /// <returns><c>True</c> is <paramref name="text"/> is valid for creating a <c>Build</c>,
        /// otherwise returns <c>False</c>.</returns>
        public static bool CheckTypedText(string text) {
            if (string.IsNullOrEmpty(text)) return false;
            return TypeCheck.IsMatch(text);
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This method is used to create a deep copy of the current <c>Build</c>.
        /// </summary>
        /// <returns>A deep copy of the current <c>Build</c>.</returns>
        public Build Copy() {
            return new Build(this);
        }

        /// <summary>
        /// This method is used to compare the current <c>Build</c> with
        /// <paramref name="otherBuild"/>.
        /// </summary>
        /// <param name="otherBuild">The <c>Build</c> you want to compare <c>this</c>
        /// build to.</param>
        /// <returns><c>1</c> if <c>this</c> <c>Build</c> is greater than <paramref name="otherBuild"/>.
        /// <c>-1</c> if <c>this</c> <c>Build</c> is less than <paramref name="otherBuild"/>.
        /// <c>0</c> if <c>this</c> <c>Build</c> is equal to <paramref name="otherBuild"/>.</returns>
        public int CompareTo([AllowNull] Build otherBuild) {
            if (otherBuild == null) return 1; //this is greater
            if (this > otherBuild) return 1; //this is greater
            if (this < otherBuild) return -1; //this is less
            return 0; //this is the same
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// This method is used to get the <c>string</c> value of
        /// <c>this</c> <c>Build</c>.
        /// </summary>
        /// <returns>A <c>string</c> value of <c>this</c> <c>Build</c>.</returns>
        public override string ToString() {
            return $"{Major}.{Minor}.{SubMinor}{TypeString}";
        }

        /// <summary>
        /// This method is used to check if <paramref name="obj"/> is equal to
        /// the current <c>Build</c>.
        /// </summary>
        /// <param name="obj">The <c>Build</c> you want to check.</param>
        /// <returns><c>True</c> if <paramref name="obj"/> is equal to the current
        /// <c>Build</c>, otherwise returns <c>False</c>.</returns>
        public override bool Equals(object obj) {
            //make sure the obj is a Build object or a string
            if (!(obj is Build) && !(obj is string) && !(obj is String)) return false;
            Build build;
            //convert string to Build
            if ((obj is string) || (obj is String)) {
                //check the string formatting
                if (!IsBuild(obj as string)) return false;
                build = new Build(obj as string);
            } else { //the object is a Build object
                build = obj as Build;
            }
            //compare the builds
            if (Major != build.Major) return false;
            if (Minor != build.Minor) return false;
            if (SubMinor != build.SubMinor) return false;
            if (Type != build.Type) return false;
            //the objects are equal
            return true;
        }

        /// <summary>
        /// This method is used to get a hash code that can be used to compare this
        /// <c>Build</c> to another.
        /// </summary>
        /// <returns>A <c>HashCode</c> of the current <c>Build</c>.</returns>
        public override int GetHashCode() {
            return this.ToString().GetHashCode(StringComparison.InvariantCulture);
        }

        #endregion

        #region Bool Operators

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is less than
        /// or equal to <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is less than or equal
        /// to <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <=(Build a, Build b) {
            if (a == b) return true;
            return b > a;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is less than
        /// or equal to <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is less than or equal
        /// to <paramref name="s"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <=(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            if (a == b) return true;
            return a <= b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="s"/> is less than
        /// or equal to <paramref name="b"/>.
        /// </summary>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="s"/> is less than or equal
        /// to <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <=(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            if (a == b) return true;
            return a <= b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is greater than
        /// or equal to <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is greater than or equal
        /// to <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >=(Build a, Build b) {
            if (a == b) return true;
            return b < a;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is greater than
        /// or equal to <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is greater than or equal
        /// to <paramref name="s"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >=(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            if (a == b) return true;
            return a >= b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="s"/> is greater than
        /// or equal to <paramref name="b"/>.
        /// </summary>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="s"/> is greater than or equal
        /// to <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >=(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            if (a == b) return true;
            return a >= b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is less than
        /// <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is less than 
        /// <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <(Build a, Build b) {
            return b > a;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is less than
        /// <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is less than
        /// <paramref name="s"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return b > a;
        }

        /// <summary>
        /// This method is used to check if <paramref name="s"/> is less than
        /// <paramref name="b"/>.
        /// </summary>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="s"/> is less than
        /// <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator <(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return b > a;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is greater than
        /// <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is greater than
        /// <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >(Build a, Build b) {
            //if b is null and a is not
            if (!(b is Build) && (a is Build)) return true;
            //if a is null or a and b are both null
            if (!(b is Build) || !(a is Build)) return false;
            //check type
            if (a.Type < b.Type) return false;
            if (a.Type > b.Type) return true;
            //check major
            if (a.Major < b.Major) return false;
            if (a.Major > b.Major) return true;
            //check minor
            if (a.Minor < b.Minor) return false;
            if (a.Minor > b.Minor) return true;
            //check subminor
            if (a.SubMinor < b.SubMinor) return false;
            if (a.SubMinor > b.SubMinor) return true;
            //the versions are the same so return false
            return false;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is greater than
        /// <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><c>True</c> if <paramref name="a"/> is greater than
        /// <paramref name="s"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return a > b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="s"/> is greater than
        /// <paramref name="b"/>.
        /// </summary>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><c>True</c> if <paramref name="s"/> is greater than
        /// <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator >(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return a > b;
        }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is not
        /// equal to <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains an <c>Object</c></param>
        /// <returns><c>True</c> if <paramref name="a"/> is not equal to
        /// <paramref name="b"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator !=(Build a, [AllowNull] object b) { return !(a == b); }

        /// <summary>
        /// This method is used to check if <paramref name="a"/> is
        /// equal to <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains an <c>Object</c></param>
        /// <returns><c>True</c> if <paramref name="a"/> is equal to
        /// <paramref name="s"/>, otherwise returns <c>False</c>.</returns>
        public static bool operator ==(Build a, [AllowNull] object s) {
            Build b = null;
            if ((s is string) && Build.IsBuild(s as string)) b = new Build(s as string);
            if (s is Build) b = s as Build;
            //handle null values
            if ((a is Build) && !(b is Build)) return false;
            if ((b is Build) && !(a is Build)) return false;
            if (!(a is Build) && !(b is Build)) return true;
            if (a == null && b == null) return true;
            if (a == null || b == null) return false;
            //check values
            if (a.Major != b.Major) return false;
            if (a.Minor != b.Minor) return false;
            if (a.SubMinor != b.SubMinor) return false;
            if (a.Type != b.Type) return false;
            //everything matches
            return true;
        }

        #endregion

        #region Arithmetic Operators

        /// <summary>
        /// This method is used to add <c>1</c> to <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <returns><paramref name="a"/>+1</returns>
        public static Build operator ++(Build a) {
            return a + Build.SubMinorBuild;
        }

        /// <summary>
        /// This method is used to add <paramref name="i"/> to <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="i">Contains an <c>int</c>.</param>
        /// <returns><paramref name="a"/>+<paramref name="i"/>.</returns>
        public static Build operator +(Build a, int i) {
            Build b = null;
            try { b = new Build(i); } catch (Exception) { }
            return a + b;
        }

        /// <summary>
        /// This method is used to add <paramref name="b"/> to <paramref name="s"/>.
        /// </summary>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="b"/>+<paramref name="s"/>.</returns>
        public static Build operator +(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return a + b;
        }

        /// <summary>
        /// This method is used to add <paramref name="s"/> to <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="a"/>+<paramref name="s"/>.</returns>
        public static Build operator +(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return a + b;
        }

        /// <summary>
        /// This method is used to add <paramref name="b"/> to <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains an <c>Build</c>.</param>
        /// <returns><paramref name="a"/>+<paramref name="b"/>.</returns>
        public static Build operator +(Build a, Build b) {
            //check null values
            if (a == null && b == null) return null;
            if (a == null) return b.Copy();
            if (b == null) return a.Copy();
            //create carryover
            int r = 0;
            //first add the sub minors
            int subMinor = a.SubMinor + b.SubMinor;
            if (subMinor > 9999) {
                r = subMinor / 9999;
                subMinor %= 10000;
            }
            //next add the minors
            int minor = a.Minor + b.Minor + r;
            r = 0;
            if (minor > 9999) {
                r = minor / 9999;
                minor %= 10000;
            }
            //next add the majors
            int major = a.Major + b.Major + r;
            if (major > 9999) throw new ArgumentOutOfRangeException();
            //return the result
            return new Build((ushort)major, (ushort)minor, (ushort)subMinor, a.Type);
        }

        /// <summary>
        /// This method is used to subtract <c>1</c> from <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <returns><paramref name="a"/>-1</returns>
        public static Build operator --(Build a) {
            return a - Build.SubMinorBuild;
        }

        /// <summary>
        /// This method is used to subtract <paramref name="i"/> from <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="i">Contains an <c>int</c>.</param>
        /// <returns><paramref name="a"/>-<paramref name="i"/>.</returns>
        public static Build operator -(Build a, int i) {
            Build b = null;
            try { b = new Build(i); } catch (Exception) { }
            return a - b;
        }

        /// <summary>
        /// This method is used to subtract <paramref name="s"/> from <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="a"/>-<paramref name="s"/>.</returns>
        public static Build operator -(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return a - b;
        }

        /// <summary>
        /// This method is used to subtract <paramref name="b"/> from <paramref name="s"/>.
        /// </summary>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="s"/>-<paramref name="b"/>.</returns>
        public static Build operator -(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return a - b;
        }

        /// <summary>
        /// This method is used to subtract <paramref name="b"/> from <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><paramref name="a"/>-<paramref name="b"/>.</returns>
        public static Build operator -(Build a, Build b) {
            //check null values
            if (a == null && b == null) return null;
            if (a == null) return b.Copy();
            if (b == null) return a.Copy();
            //first subtract the values
            int subMinor = (int)a.SubMinor - (int)b.SubMinor;
            int minor = (int)a.Minor - (int)b.Minor;
            int major = (int)a.Major - (int)b.Major;
            //first handle subMinor
            while (subMinor < 0) {
                minor -= 1;
                subMinor += 10000;
            }
            //next handle minor
            while (minor < 0) {
                major -= 1;
                minor += 10000;
            }
            //if major is less than 0 throw exception
            if (major < 0) throw new ArgumentOutOfRangeException();
            //return the result
            return new Build((ushort)major, (ushort)minor, (ushort)subMinor, a.Type);
        }

        /// <summary>
        /// This method is used to multiply <paramref name="i"/> with <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="i">Contains an <c>int</c>.</param>
        /// <returns><paramref name="a"/>*<paramref name="i"/>.</returns>
        public static Build operator *(Build a, int i) {
            Build b = null;
            if (i > -1 && i < 10000) {
                b = new Build((ushort)i, (ushort)i, (ushort)i);
            }
            return a * b;
        }

        /// <summary>
        /// This method is used to multiply <paramref name="a"/> with <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="a"/>*<paramref name="s"/>.</returns>
        public static Build operator *(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return a * b;
        }

        /// <summary>
        /// This method is used to multiply <paramref name="s"/> with <paramref name="b"/>.
        /// </summary>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="s"/>*<paramref name="b"/>.</returns>
        public static Build operator *(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return a * b;
        }

        /// <summary>
        /// This method is used to multiply <paramref name="b"/> with <paramref name="a"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><paramref name="a"/>*<paramref name="b"/>.</returns>
        public static Build operator *(Build a, Build b) {
            //check null values
            if (a == null && b == null) return null;
            if (a == null) return b.Copy();
            if (b == null) return a.Copy();
            //multiply the values and clamp between 0 and 9999
            int subMinor = Math.Clamp(((int)a.SubMinor) * ((int)b.SubMinor), 0, 9999);
            int minor = Math.Clamp(((int)a.Minor) * ((int)b.Minor), 0, 9999);
            int major = Math.Clamp(((int)a.Major) * ((int)b.Major), 0, 9999);
            //return result
            return new Build((ushort)major, (ushort)minor, (ushort)subMinor, a.Type);
        }

        /// <summary>
        /// This method is used to divide <paramref name="a"/> by <paramref name="i"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="i">Contains an <c>int</c>.</param>
        /// <returns><paramref name="a"/>/<paramref name="i"/>.</returns>
        public static Build operator /(Build a, int i) {
            Build b = null;
            if (i > -1 && i < 10000) {
                b = new Build((ushort)i, (ushort)i, (ushort)i);
            }
            return a / b;
        }

        /// <summary>
        /// This method is used to divide <paramref name="a"/> by <paramref name="s"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="a"/>/<paramref name="s"/>.</returns>
        public static Build operator /(Build a, string s) {
            Build b = null;
            if (Build.IsBuild(s)) b = new Build(s);
            return a / b;
        }

        /// <summary>
        /// This method is used to divide <paramref name="s"/> by <paramref name="b"/>.
        /// </summary>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <param name="s">Contains a <c>string</c>.</param>
        /// <returns><paramref name="s"/>/<paramref name="b"/>.</returns>
        public static Build operator /(string s, Build b) {
            Build a = null;
            if (Build.IsBuild(s)) a = new Build(s);
            return a / b;
        }

        /// <summary>
        /// This method is used to divide <paramref name="a"/> by <paramref name="b"/>.
        /// </summary>
        /// <param name="a">Contains a <c>Build</c>.</param>
        /// <param name="b">Contains a <c>Build</c>.</param>
        /// <returns><paramref name="a"/>/<paramref name="b"/>.</returns>
        public static Build operator /(Build a, Build b) {
            //check null values
            if (a == null && b == null) return null;
            if (a == null) return b.Copy();
            if (b == null) return a.Copy();
            //divide if b value is not 0 and clamp between 0 and 9999
            int subMinor = (b.SubMinor == 0) ? b.SubMinor : Math.Clamp(
                ((int)Math.Round(((double)a.SubMinor) / ((double)b.SubMinor), 0)), 0, 9999);
            int minor = (b.Minor == 0) ? b.Minor : Math.Clamp(
                ((int)Math.Round(((double)a.Minor) / ((double)b.Minor), 0)), 0, 9999);
            int major = (b.Major == 0) ? b.Major : Math.Clamp(
                ((int)Math.Round(((double)a.Major) / ((double)b.Major), 0)), 0, 9999);
            //return result
            return new Build((ushort)major, (ushort)minor, (ushort)subMinor, a.Type);
        }

        #endregion

    }
}
